/*! \file  acceptanceTest.h
 *
 *  \brief Function prototypes and manifest constants
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 5:12 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef ACCEPTANCETEST_H
#define	ACCEPTANCETEST_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! PLLFBD value for 70 MIPS */
#define DEFAULT_SPEED 74 // 70MIPS

/*! initialize() - Set up the clock and ports and initialize the LCD */
void initialize(void);
/*! testButtons() - Check that button presses can be detected */
void testButtons(void);
/*! checkButtons() - Warn if all buttons are not high */
void checkButtons(void);
/*! testLCD() - Exercise the LCD functions */
void testLCD(void);
/*! testLED() - Cycle the LEDs */
void testLED(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ACCEPTANCETEST_H */

