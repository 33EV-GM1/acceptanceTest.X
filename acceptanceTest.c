/*! \file  acceptanceTest.c
 *
 *  \brief Exercise functions of the dsPIC-EL-GM board
 * 
 * Program tests the LCD, buttons and LEDs on the dsPIC-EL-GM.  Also
 * used the deadman timer to restart the program after the final test
 * (buttons).
 * 
 * Tests included
 * \li check buttons for shorts
 * \li cycle LEDs five times
 * \li test LCD
 * \li test buttons
 * \li fire the deadman timer
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 5:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/dsPIC-EL-GM.h"
#include "../include/LCD.h"
#include "acceptanceTest.h"

//! Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
//! Watchdog timer off
#pragma config FWDTEN = OFF
//! Deadman timer off
#pragma config DMTEN = DISABLE
//! Deadman timer expiration count, high word
#pragma config DMTCNTH = 10000

/*! main - Initial testing of dsPIC-EL-GM */

/*! Runs a number of initial tests on the dsPIC-EL-GM
 *
 */
int main(void)
{
  int i;
  char szWork[32];
  
  /* Initialize the ports and LCD */
  initialize();
  
  /* Display a startup banner */
  LCDclear();
  LCDputs("  dsPIC-EL-GM   ");
  LCDline2();
  LCDputs("      Test      ");
  Delay_ms(2000);

  /* Do a quick check of the buttons before starting */
  checkButtons();
  LCDclear();

  LCDclear();
  LCDputs("     Cycle      ");
  LCDline2();
  LCDputs("     LEDs       ");
  Delay_ms(1000);
  LCDclear();
  for ( i=0; i<5; i++ )
    testLED();
  
  /* Exercise the LCD */
  testLCD();

  /* Do a test of the button presses */
  testButtons();
    LCDline2();
  LCDputs("    D O N E     ");
  Delay_ms(1000);

  /* Turn on the deadman timer to reset after we do this a while */
  DMTCONbits.ON = 1;
  
  while(1)
    {
      LCDposition(0x40);
      LCDputs("  Deadman Test   ");
      sprintf(szWork,"     %5u     ",DMTCNTH);
      LCDhome();
      LCDputs(szWork);
      //Delay_ms(100);
    }

  return 0;
}
