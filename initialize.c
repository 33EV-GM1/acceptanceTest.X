/*! \file  initialize.c
 *
 *  \brief Set up the clock and ports and initialize the LCD
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 5:09 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/dsPIC-EL-GM.h"
#include "../include/LCD.h"
#include "acceptanceTest.h"

/*! initialize() - Set up the clock and ports and initialize the LCD */

/*! Delays for 2 seconds, then sets the processor clock to 140 MHz
 *  (70 MIPS).  turns off analog on RA4, RB8 and RB9.  Sets the LED
 *  pins to be outputs.  Initializes the LCD.
 */
void initialize(void)
{
  // Wait a while to prevent programmer from doing weird stuff
  Delay_ms(2000L*23L/70L);  // Should be close to 2 second

  // Default clock
  CLKDIVbits.FRCDIV = 0;    // Divide by 1 = 8MHz
  CLKDIVbits.PLLPRE = 0;    // Divide by 2 = 4 MHz
  PLLFBD = DEFAULT_SPEED;   // Multiply by 74 = 280
  CLKDIVbits.PLLPOST = 0;   // Divide by 2 = 140
  
  /* SW1 (RB5) has no ANSEL bit */
  ANSELBbits.ANSB8 = 0;
  ANSELBbits.ANSB9 = 0;
  ANSELAbits.ANSA4 = 0;

  /* Set the LED pins to be outputs */
  LED1_TRIS = 0;
  LED2_TRIS = 0;
  LED3_TRIS = 0;

  /* Initialize the LCD */
  LCDinit();

}
