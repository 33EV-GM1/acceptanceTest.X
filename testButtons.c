/*! \file  testButtons.c
 *
 *  \brief Check that button presses can be detected
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 5:23 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "../include/dsPIC-EL-GM.h"
#include "../include/LCD.h"

int nButState[4];

/*! testButtons() - Check that button presses can be detected */

/*! Advises the user to test buttons.  A place on the display is
 *  reserved for each button.  If SW1 is pressed, a 'U' is displayed.
 *  SW2='L', SW3='R', SW4='D'.
 * 
 *  While the button states are displayed an on-display counter
 *  counts down from 99.  When zero is reached, the test ends.
 */
void testButtons( void )
{
  char szWork[32];
  int downcount;

  /* Set the initial button states to the current state */
  nButState[0]= BUTTON_UP;
  nButState[1]= BUTTON_LEFT;
  nButState[2]= BUTTON_RIGHT;
  nButState[3]= BUTTON_DOWN;
  
  /* Instruction to press buttons */
  LCDclear();
  LCDline2();
  LCDputs("Press buttons");
  
  /* Initialize the timer */
  T1CON = 0;
  PR1 = 10000;
  _T1IF = 0;
  T1CONbits.TCKPS = 3;
  T1CONbits.TON = 1;
  
  /* The timer is too fast, so set up a counter to add a few more bits */
  downcount = 1000;
  
  /* Button state will appear between the dashes */
  LCDhome();
  LCDputs("  - -- -- -- -  ");
  
  /* Do this for a fairly long time */
  while ( downcount )
    {
      /* Check whether each button is changed, if so, change the display */
      if (nButState[0] != BUTTON_UP)
        {
          LCDposition(3);
          if (BUTTON_UP)
            LCDputs("U");
          else
            LCDputs(" ");
          nButState[0] = BUTTON_UP;
        }
      if (nButState[1] != BUTTON_LEFT)
        {
          LCDposition(6);
          if (BUTTON_LEFT)
            LCDputs("L");
          else
            LCDputs(" ");
          nButState[1] = BUTTON_LEFT;
        }
      if (nButState[2] != BUTTON_RIGHT)
        {
          LCDposition(9);
          if (BUTTON_RIGHT)
            LCDputs("R");
          else
            LCDputs(" ");
          nButState[2] = BUTTON_RIGHT;
        }
      if (nButState[3] != BUTTON_DOWN)
        {
          LCDposition(12);
          if (BUTTON_DOWN)
            LCDputs("D");
          else
            LCDputs(" ");
          nButState[3] = BUTTON_DOWN;
        }
      
      /* Show the time remaining every 10 expirations of the timer */
      if ( (downcount%10) == 0)
        {
          sprintf(szWork,"%3d",downcount/10);
          LCDposition(0x4d);
          LCDputs(szWork);
        }
      
      /* If the timer has expired, bump the counter */
      if ( _T1IF )
        {
          downcount--;
          _T1IF = 0;
        }
    }
}
