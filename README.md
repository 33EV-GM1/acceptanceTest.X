# dsPIC-EL-GM Acceptance Test

This application performs a number of simple test on a dsPIC-EL-GM with a
dsPIC33EVxxGMy02 installed.

Tests include:
* Initial test for shorted buttons
* Cycle LEDs
* Test many LCD functions
* Test buttons (requires user engagement)
* Test the deadman timer

This application is programmed into the dsPIC33EV32GM002 parts provided
in the kits for the Electronics and Wireless Communications Club.
